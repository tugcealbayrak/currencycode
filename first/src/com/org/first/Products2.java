package com.org.first;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Products2 {
	
	 @SuppressWarnings("unlikely-arg-type")
	public static void main(String argv[]) {
		 double convert;
		 double price;

		 try { //xml parse
			 	List<String> strings = new ArrayList<>();
				File xml = new File("products2.xml");
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(xml);
				doc.getDocumentElement().normalize();
				System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
				
				//Product tagName i NodeList icinde tutularak xml'den json'a verilerin parse islemi yapilir.
				NodeList nodeList = doc.getElementsByTagName("Product");

				for (int i = 0; i < nodeList.getLength(); i++) {
					Node nNode = nodeList.item(i);
					System.out.println("Current Element :" + nNode.getNodeName());
							
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						strings.add(eElement.getAttribute("Name"));
						
						/*Name kontrolu var.Gelen Name list'e aktarilir ve NodeList icindeki Name ile karsilažtirilir.
						 * Eger ayni Name'den baska varsa ekrana bir yazi yazar.
						 * Ayni Name'den yoksa diger verileri parse eder.
						 */
						if(nodeList.equals(strings) == true) {
							System.out.println("Ayni Name'den var.");
							
						} else {
						System.out.println("Product Name : " + eElement.getAttribute("Name"));
						
						/*Parse edilecek Price tag'i bos degilse islem devam eder ve price degeri double deger olarak
						 * tutulur.*/
						if(eElement.getElementsByTagName("Price").item(0).getTextContent() != null) {
							System.out.println("Price : " + eElement.getElementsByTagName("Price").item(0).getTextContent());
						    price = Double.parseDouble(eElement.getElementsByTagName("Price").item(0).getTextContent());
						    
						    //Price tag'i parse edilir.
							NodeList list = doc.getElementsByTagName("Price");
							Node node = list.item(i);
				
							if(node.getNodeType() == Node.ELEMENT_NODE) {
								Element element = (Element) node;
								System.out.println("Currency : " + element.getAttribute("Currency"));
								String currency = element.getAttribute("Currency");
								
								/* Currency degeri alinarak EUR kontrolu yapilir.Eger EUR'ya esit degilse 
								 * CurrencyConventer sinifindaki convert metodu cagirilarak,parse edilen currency ile
								 * donusturulmesi gereken currency degeri yazilir.Gelen sonuc,alinan price degerinin EUR'ya donusturulmus
								 * haliyle ekrana yazilir. */
								if(currency != "EUR") {
									convert = CurrencyConventer.convert(currency, "EUR");
									System.out.println("Price "+price +" "+ currency+" eždešeri "+(convert * price)+" "+"EUR");
								}
									//Price icindeki BestBefore tag'i parse olur ve degeri yazdirilir.
									NodeList nodeList2 = doc.getElementsByTagName("BestBefore");
									Node node2 = nodeList2.item(i);
									
									if(node2.getNodeType() == Node.ELEMENT_NODE) {
										Element element2 = (Element) node2;
										System.out.println("BestBefore : " + element2.getAttribute("Date"));
										System.out.println("--------------");
								}					
							}
						
						
						}else {
							//eger price degeri null ise deger sifirlanir ve CurrencyConventer.convert metodu cagirilir.
							price = 0.0;
							convert = CurrencyConventer.convert("", "EUR");
							System.out.println("--------------");
							}	
						}
					}
				}
				
			 } catch (Exception e) {
				e.printStackTrace();
		 }
	}
}
