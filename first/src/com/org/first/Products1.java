package com.org.first;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Products1 {
	 @SuppressWarnings("unlikely-arg-type")
	public static void main(String argv[]) {
		 try {
			List<String> strings = new ArrayList<>();
		 	File xml = new File("products1.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xml);		
			doc.getDocumentElement().normalize();
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
				
			//Product tagName i NodeList icinde tutularak xml'den json'a verilerin parse islemi yapilir.
			NodeList nList = doc.getElementsByTagName("Product");
			
			for(int i=0; i<nList.getLength(); i++) {
				Node nNode = nList.item(i);
				System.out.println("Current Element :" + nNode.getNodeName());
				
				if(nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					strings.add(eElement.getElementsByTagName("Name").item(0).getTextContent());
				
					/*Name kontrolu var.Gelen Name list'e aktarilir ve NodeList icindeki Name ile karsilažtirilir.
					 * Eger ayni Name'den baska varsa ekrana bir yazi yazar.
					 * Ayni Name'den yoksa diger verileri parse eder.
					 */
					if(nList.equals(strings) == true) {
						System.out.println("Ayni Name den var");
						
					} else {
						//veriler parse edilir ve ekrana yazdirilir.					
						System.out.println("Name : " + eElement.getElementsByTagName("Name").item(0).getTextContent());
						System.out.println("BestBefore : " + eElement.getAttribute("BestBefore"));
						System.out.println("Price : " + eElement.getElementsByTagName("Price").item(0).getTextContent());
						System.out.println("Currency : " + eElement.getElementsByTagName("Currency").item(0).getTextContent());
						String currency = eElement.getElementsByTagName("Currency").item(0).getTextContent();
						double price = Double.parseDouble(eElement.getElementsByTagName("Price").item(0).getTextContent());
					
						/* Currency degeri alinarak EUR kontrolu yapilir.Eger EUR'ya esit degilse 
						 * CurrencyConventer sinifindaki convert metodu cagirilarak,parse edilen currency ile
						 * donusturulmesi gereken currency degeri yazilir.Gelen sonuc, alinan price degerinin EUR'ya donusturulmus
						 * haliyle ekrana yazilir. */
						if(currency != "EUR")
							{
								double convert = CurrencyConventer.convert(currency, "EUR");
								System.out.println("Price "+price +" "+ currency+" eždešeri "+(convert * price)+" "+"EUR");
								System.out.println("----------------------------");
							}
					}
				
				}
				
			}
			
		 }catch (Exception e) {
			 e.printStackTrace();
		}
		 	
		 }
}
