package com.org.first;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Products3 {

	@SuppressWarnings("unlikely-arg-type")
	public static void main(String[] args) {
		try {
		List<String> strings = new ArrayList<>();
		File xml = new File("products3.xml");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(xml);
		document.getDocumentElement().normalize();
		System.out.println("Root element :" + document.getDocumentElement().getNodeName());
		
		//Item tagName i NodeList icinde tutularak xml'den json'a verilerin parse islemi yapilir.
		NodeList list = document.getElementsByTagName("Item");
		
		for(int temp = 0; temp < list.getLength(); temp++) {
			Node node = list.item(temp);
			System.out.println("Current Element :" + node.getNodeName());
			
			// Item Id tag'i parse edilir.
			if(node.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) node;
				System.out.println("Id : " + eElement.getElementsByTagName("Id").item(0).getTextContent());
				}
			
			//Product tag'i parse edilir.
			NodeList list2 = document.getElementsByTagName("Product");
			Node node2 = list2.item(temp);
			if(node2.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node2;
				strings.add(element.getElementsByTagName("Name").item(0).getTextContent());
			
				/*Name kontrolu var.Gelen Name list'e aktarilir ve NodeList icindeki Name ile karsilažtirilir.
				 * Eger ayni Name'den baska varsa ekrana bir yazi yazar.
				 * Ayni Name'den yoksa diger verileri parse eder.
				 */
				if(list2.equals(strings) == true) {
					System.out.println("Ayni Name'den var");
					
				} else {
			
				System.out.println("Name : " +element.getElementsByTagName("Name").item(0).getTextContent());
				String currency = element.getElementsByTagName("Currency").item(0).getTextContent();
				double price = Double.parseDouble(element.getElementsByTagName("Price").item(0).getTextContent());
				
				/* Currency degeri alinarak EUR kontrolu yapilir.Eger EUR'ya esit degilse 
				 * CurrencyConventer sinifindaki convert metodu cagirilarak,parse edilen currency ile
				 * donusturulmesi gereken currency degeri yazilir.Gelen sonuc, alinan price degerinin EUR'ya donusturulmus
				 * haliyle ekrana yazilir. */
				if(currency != "EUR")
				{
					double convert = CurrencyConventer.convert(currency, "EUR");
					System.out.println("Price "+price +" "+ currency+" eždešeri "+(convert * price)+" "+"EUR");
					System.out.println("--------------");
				}
			}
			}
			}
			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
