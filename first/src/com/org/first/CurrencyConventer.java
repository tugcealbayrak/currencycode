package com.org.first;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gson.Gson;

public class CurrencyConventer {	
		// API URL
		private static final String API_PROVDIER = "http://api.fixer.io/";

		//convert metoduyla API'den gelen son verilere gore fromCurrency ile toCurrency verilerini sonuc olarak isler.
		public static double convert(String fromCurrency, String toCurrency) {

			if ((fromCurrency != null && !fromCurrency.isEmpty())
					&& (toCurrency != null && !toCurrency.isEmpty())) {

				Currency response = getResponse(API_PROVDIER+"/latest?base="+fromCurrency);
				
				if(response != null) {
					
					String rate = response.getRates().get(toCurrency);
					double conversionRate = Double.valueOf((rate != null)?rate:"0.0");
					
					return conversionRate;
				}
				
			}

			return 0.0;
		}

		// API'den sonuclari alan metot
		private static Currency getResponse(String sUrl) {

			Currency response = null;
			Gson gson = new Gson();
			StringBuffer buffer = new StringBuffer();
			
			if(sUrl == null || sUrl.isEmpty()) {
				
				System.out.println("Application Error");
				return null;
			}

			URL url;
			try {
				url = new URL(sUrl);

				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				InputStream stream = connection.getInputStream();
				int data = stream.read();

				while (data != -1) {

					buffer.append((char) data);
					data = stream.read();
				}

				stream.close();
				response = gson.fromJson(buffer.toString(), Currency.class);

			} catch (MalformedURLException e) {

				System.out.println(e.getMessage());
				e.printStackTrace();
				
			} catch (IOException e) {

				System.out.println(e.getMessage());
				e.printStackTrace();
			}

			return response;
		}

		@SuppressWarnings("unused")
		public static void main(String[] args) throws IOException {
 
			/*Product siniflarindan gelen fromCurrency ile cevirilmesi istenen toCurrency verileri 
			  convert metoduna yerlestirilerek API'den gelen son verilere gore islem yapar.*/
		    String fromCurrency = null;
			String toCurrency = null;

			double currencyConvert = convert(fromCurrency, toCurrency);
		}

}
